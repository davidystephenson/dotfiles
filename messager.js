const fs = require('fs')

function empty (lines, key) {
  const line = lines.find(line => line.includes(key))

  return line === `${key}=""`
}

const file = fs.readFileSync('./messagetypes.txt', 'utf8')
console.log('file test:', file)
const types = file.split('}')
// console.log('types test:', types)

const changed = types.map(type => {
  const lines = type.split('\r\n\t')

  const picture = !empty(lines, 'picture')
  const background = !empty(lines, 'background_picture')

  const full = picture || background
  console.log('full test:', full)

  const priority = full ? 1 : 2

  const prioritized = lines.map(line => {
    if (!line.includes('retain')) {
      if (line.includes('priority')) {
        const start = line.slice(0, -1)

        return `${start}${priority}`
      }
    }

    if (line.includes('pause')) {
      const end = line.slice(-2)

      if (end === 'no') {
        const start = line.slice(0, -2)

        return `${start}yes`
      }
    }

    return line
  })

  return prioritized.join('\r\n\t')
})

const joined = changed.join('}')

fs.writeFileSync('changed.txt', joined)
