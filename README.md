# dotfiles

dotfiles is my personal set of configurations for Ubuntu. You can follow the instructions to set up a Regolith 3.2 24.04 installation.

## Instructions

1. Install [zsh](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH), `git-core`, and [Oh-My-Zsh](https://github.com/robbyrussell/oh-my-zsh).
1. Create a `~/Sync` directory.
1. Clone this repository inside the `Sync` directory.
1. Replace your `~/.zshrc` with the contents of '~/Sync/dotfiles/zshrc', i.e., `cp ~/Sync/dotfiles/zshrc ~/.zshrc`.
1. Follow [kgilmer's instructions](https://github.com/regolith-linux/regolith-desktop/issues/743#issuecomment-2016605687) to disable the sleep keybindings.
1. Install OBS.
1. Run `echo "v4l2loopback" | sudo tee /etc/modules-load.d/v4l2loopback.conf` to enable OBS virtual camera.
