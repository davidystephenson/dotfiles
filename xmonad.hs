import XMonad
import XMonad.Config.Xfce
import XMonad.Util.EZConfig
import qualified XMonad.StackSet as W

resoSwitch :: X ()
resoSwitch = do
  ws <- gets windowset
  let wsID = W.tag . W.workspace . W.current $ ws
  let (S scrnID) = W.screen . W.current $ ws
  spawn ("/home/david/Sync/bin/xmonad/clarify.sh " ++ show wsID ++ " " ++ show scrnID)

main = xmonad $ xfceConfig { terminal = "gnome-terminal", modMask = mod4Mask, logHook = resoSwitch }
  -- `additionalKeys`
  -- [ ((0, xK_KP_End        ), spawn "/home/david/Sync/bin/roccat/character1.sh")
  -- , ((0, xK_KP_Down        ), spawn "/home/david/Sync/bin/roccat/character2.sh")
  -- , ((0, xK_KP_Left        ), spawn "/home/david/Sync/bin/roccat/big1.sh")
  -- , ((0, xK_KP_Begin        ), spawn "/home/david/Sync/bin/roccat/goto.sh")
  -- , ((0, xK_KP_Right        ), spawn "/home/david/Sync/bin/roccat/next.sh")
  -- , ((0, xK_KP_Home        ), spawn "/home/david/Sync/bin/roccat/small.sh")
  -- , ((0, xK_KP_Up        ), spawn "/home/david/Sync/bin/roccat/big2.sh")
  -- ]
