" todo: colon to space, caps lock to escape

set smartindent
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

set fileencodings=ucs-bom,utf-8,cp936,big5,euc-jp,euc-kr,gb18030,latin1 " For multi-byte character support (CJK support, for example):

set showcmd " Show (partial) command in status line.

set incsearch " While typing a search command, show immediately where the so far typed pattern matches.

set ignorecase  " Ignore case in search patterns.

set smartcase " Override the 'ignorecase' option if the search pattern contains upper case characters.

set ruler " Show the line and column number of the cursor position, separated by a comma.

set wrapscan " With this option the search next behaviour allows to jump to the beginning of the file, when the end of file is reached. Similarly, search previous jumps to the end of the file when the start is reached.

set whichwrap=b,s,<,>,[,] " Traverse lines using arrow keys

set number " Show line numbers

if has("autocmd") " Cursor appears in previous position after reopening file
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

set viminfo='10,\"100,:20,%,n~/.viminfo "Tell vim to remember certain things when we exit:
""100 :  will save up to 100 lines for each register
":20  :  up to 20 lines of command-line history will be remembered
"%    :  saves and restores the buffer list
"n... :  where to save the viminfo files

set background=dark

"Vim -latex-suite options, from http://vim-latex.sourceforge.net/documentation/latex-suite/recommended-settings.html
set grepprg=grep\ -nH\ $* "grep will sometimes skip displaying the file name if you search in a singe file. This will confuse Latex-Suite. Set your grep program to always generate a file-name.

let g:tex_flavor='latex' "Starting with Vim 7, the filetype of empty .tex files defaults to 'plaintex' instead of 'tex', which results in vim-latex not being loaded. The following changes the default filetype back to 'tex':

" Enable vim indent guides
let g:indent_guides_enable_on_vim_startup = 1

" This option holds all the filetypes for which MatchTagAlways  will try to find and highlight enclosing tags.
let g:mta_filetypes = { 'html' : 1, 'xhtml' : 1, 'xml' : 1, 'jinja' : 1, 'html.handlebars' : 1 }

filetype plugin indent on "These two enable syntax highlighting
syntax on

" Fold settings
set foldmethod=indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set foldenable        "dont fold by default
set foldlevel=1         "this is just what i use

" Use the system clipboard
set clipboard=unnamedplus

"" Key Mappings
" Map , to leader
let mapleader=','

" Map space to the colon when not in insert mode
noremap <space> :

" That will map Ctrl+C whilst still within insert mode to the line
" splitting command. You can obviously rebind it if you want,
" but I think this works really well.
imap <C-c> <CR><Esc>O

" Insert a new line above without going into insert mode with Control-o
nnoremap OO O<Esc>

" Insert a new line below without going into insert mode with Control-o
nnoremap oo o<Esc>

" Fix python syntax highlighting and indenting
autocmd BufNewFile,BufRead .py,.pyw set filetype=python

" Map jj to escape in insert mode
inoremap jj <Esc>

" Use Control + movement keys for window focus movement
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Normal backspace behavior for the default compilation
set backspace=indent,eol,start

xnoremap p "_dP
noremap d "0d
noremap dd "0dd
noremap x "+x
noremap d$ D
noremap D "+dd

" Disable beep sound
set noeb vb t_vb=

" parcel.js https://parceljs.org/hmr.html#safe-write 
set backupcopy=yes
